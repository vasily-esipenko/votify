# Votify

Приложение для создание опросов и участия в них

Создано при помощи:
* Python, Django (фреймворк)
* HTML, CSS
* SQLite3 (база данных)

При создании использовалась информация с ресурсов:
* [django documentation](https://docs.djangoproject.com/en/3.2/)
* [w3schools](https://www.w3schools.com/)
* [MDN Web Docs](https://developer.mozilla.org/en-US/)
* [YouTube](https://www.youtube.com/)
* [StackOverflow](https://stackoverflow.com/)
